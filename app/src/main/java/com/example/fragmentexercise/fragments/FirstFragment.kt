package com.example.fragmentexercise.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.fragmentexercise.R

class FirstFragment : Fragment(R.layout.fragment_first) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val buttonSend = view.findViewById<Button>(R.id.buttonSend)
        val editTextAmount = view.findViewById<EditText>(R.id.editTextAmount)

        val controller = Navigation.findNavController(view)

        buttonSend.setOnClickListener{
            val amountText = editTextAmount.text.toString()
            if (amountText.isEmpty()){
                return@setOnClickListener
            }
            val amount = amountText.toInt()
            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(amount)
            controller.navigate(action)
        }
    }
}